var fs = require('fs');
var os = require('os');
var auth0 = require('auth0-js');
var readline = require('readline-sync');
var request = require('request');
var util = require('util');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0" // Avoids DEPTH_ZERO_SELF_SIGNED_CERT error for self-signed certs

function url() {

  var merge_dir = os.homedir()+"/.merge";
  var url_path = merge_dir+"/portal";
  try {
    var portal = fs.readFileSync(url_path);
    return "https://"+portal;
  }
  catch (e) {
    console.log("You must specify a portal address via ./mc init");
    process.exit(1);
  }

}

function withLogin(f, overwrite=false) {
  var merge_dir = os.homedir()+"/.merge";
  var token_path = merge_dir+"/token";

  if(!fs.existsSync(merge_dir)) {
    fs.mkdirSync(merge_dir);
  }

  if(!fs.existsSync(token_path) || overwrite) { doLogin(f); }
  else {
    f(fs.readFileSync(token_path));
  }
  
}

function doLogin(f) {

  var email = readline.question('email: ');
  var passwd = readline.question('password: ', {
    hideEchoBack: true
  });
  var auth = new auth0.WebAuth({
    domain: 'mergetb.auth0.com',
    audience: 'https://mergetb.net/api/v1',
    scope: 'openid profile email',
    clientID: 'opDBVn3y2Ez4g43c128hDvEaaO6si0na'
  });
  var merge_dir = os.homedir()+"/.merge";
  var token_path = merge_dir+"/token";

  auth.client.login({
    realm: 'Username-Password-Authentication',
    username: email,
    password: passwd
  }, function(err, result) {
    if(err) {
      console.log("login failed: ", err.description);
    }
    else {
      fs.writeFileSync(token_path, result.accessToken);
      f(result.accessToken);
    }
  });

}

function putStuff(path, body, f) {
    withLogin(function(access_token) {
        var options = {
            url: url()+path,
            headers: {
                'authorization': 'Bearer ' + access_token
            },
            json: true,
            body: body,
        };
        var callback = function(error, response, body) {
            if(error != null) {
                console.log('portal api call failed: ', error);
            }
            // a 401 probably means the token expired, so refresh and try again
            else if(response.statusCode == 401) {
                withLogin(function(access_token) {
                    options.headers.authorization = 'Bearer ' + access_token;
                    request.put(options, callback)
                }, true);
                return;
            }
            else if(response.statusCode != 200) {
                console.log('bad status: ', response.statusCode);
            }
            else {
                // stuff = JSON.parse(body);
                f(response.statusCode, body); 
            }
        }
        request.put(options, callback);
    });
}

function postStuff(path, body, f) {
    withLogin(function(access_token) {
        var options = {
            url: url()+path,
            headers: {
                'authorization': 'Bearer ' + access_token
            },
            json: true,
            body: body,
        };
        var callback = function(error, response, body) {
            if(error != null) {
                console.log('portal api call failed: ', error);
            }
            // a 401 probably means the token expired, so refresh and try again
            else if(response.statusCode == 401) {
                withLogin(function(access_token) {
                    options.headers.authorization = 'Bearer ' + access_token;
                    request.put(options, callback)
                }, true);
                return;
            }
            else if(response.statusCode != 200) {
                console.log('bad status: ', response.statusCode);
            }
            else {
                // stuff = JSON.parse(body);
                f(response.statusCode, body); 
            }
        }
        request.post(options, callback);
    });
}

function getStuff(path, f) {

  withLogin(function(access_token) {

    var options = {
      url: url()+path,
      headers: {
        'authorization': 'Bearer ' + access_token
      }
    };

    var callback = function(error, response, body) {
      if(error != null) {
        console.log('portal api call failed: ', error);
      }
      // a 401 probably means the token expired, so refresh and try again
      else if(response.statusCode == 401) {
        withLogin(function(access_token) {
          options.headers.authorization = 'Bearer ' + access_token;
          request.get(options, callback)
        }, true);
        return;
      }
      else if(response.statusCode != 200) {
        console.log('bad status: ', response.statusCode)
        console.log('body \n', response.body)
      }
      else {
        stuff = JSON.parse(body);
        f(stuff); 
      }

    }

    request.get(options, callback);
  });

}

function delStuff(path, f) {

  withLogin(function(access_token) {

    var options = {
      url: url()+path,
      headers: {
        'authorization': 'Bearer ' + access_token
      }
    };

    var callback = function(error, response, body) {
      if(error != null) {
        console.log('portal api call failed: ', error);
      }
      // a 401 probably means the token expired, so refresh and try again
      else if(response.statusCode == 401) {
        withLogin(function(access_token) {
          options.headers.authorization = 'Bearer ' + access_token;
          request.get(options, callback)
        }, true);
        return;
      }
      else if(response.statusCode != 200) {
        console.log('bad status: ', response.statusCode)
        pretty(body)
      }
      else {
        //stuff = JSON.parse(body);
        f(response.statusCode, body); 
      }

    }

    request.delete(options, callback);
  });

}

function pretty(thing) {
  console.log(util.inspect(thing, {compact: false, colors: true, depth: 20}))
}

module.exports = {
  url,
  withLogin,
  putStuff,
  getStuff,
  postStuff,
  delStuff,
  pretty
}
